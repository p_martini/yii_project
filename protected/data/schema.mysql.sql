-- MySQL dump 10.14  Distrib 5.5.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: profit
-- ------------------------------------------------------
-- Server version	5.5.34-MariaDB-1~wheezy

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `parent` mediumint(8) unsigned NOT NULL,
  `keywords` varchar(69) NOT NULL,
  `description` varchar(160) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `parent` (`parent`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Broker reviews','broker-reviews',0,'Binary opitions broker review','',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` mediumint(8) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `text` text NOT NULL,
  `ip` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page` (`page_id`),
  KEY `uid` (`uid`),
  KEY `status` (`status`) USING HASH,
  KEY `fk_comment_1` (`page_id`),
  CONSTRAINT `fk_comment_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_block`
--

DROP TABLE IF EXISTS `content_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_block` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_block`
--

LOCK TABLES `content_block` WRITE;
/*!40000 ALTER TABLE `content_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` mediumint(8) unsigned NOT NULL,
  `url` tinytext NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_media_1` (`page`),
  KEY `type` (`type`),
  CONSTRAINT `fk_media_1` FOREIGN KEY (`page`) REFERENCES `page` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (6,2,'1392859765Anyoption_logo_jpg.jpg',1);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `category` mediumint(8) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(160) NOT NULL,
  `keywords` varchar(69) NOT NULL,
  `view` varchar(20) NOT NULL DEFAULT 'index',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`) USING HASH,
  KEY `active` (`active`),
  KEY `fk_page_1` (`category`),
  KEY `updated` (`updated`),
  KEY `created` (`created`),
  CONSTRAINT `fk_page_1` FOREIGN KEY (`category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (2,1,'Anyoption Review','anyoption-review','<p>AnyOption Binary Options Brokers</p>\r\n\r\n<p>You Should! You Should!</p>\r\n\r\n<p>One of the best things about AnyOption is that they are fully regulated. As one of the first real pioneers in the binary options industry, this company now leads in the term of innovations. AnyOption provides just the right set of tools and healthy refunds. The really good thing is that they do not have any tricky bonuses or other fairytales. This allows the trader to enjoy trading with them and to be without worries or concerns. For the newbies, up to a 25% refund makes it really nice.</p>\r\n\r\n<p>Should you? Should You?</p>\r\n\r\n<p>The secret is out that AnyOption&rsquo;s is an example of many of the fresh binary options brokers. They have trading tools which are just waiting to be used and also new trading assets which are made available to the traders each day. They are only average in regards to their returns and have an average return of around 75%.</p>\r\n\r\n<p>Full Review of AnyOptions</p>\r\n\r\n<p>AnyOptions was started in 2008. They rightfully claim the title of the first really true binary options pioneer in the industry. They were the first company to see the advantage of trading binary options and took this opportunity in hand. After many years of experience, the AnyOption team developed the first binary options 100% web based platform for the trading of binary options.</p>\r\n\r\n<p>Because this platform was the first and also the most unique platform, they were the catalyst of many other platforms, created by different companies. Although other companies have attempted to mimic their platform, they still remain distinguished from the others. SpotOption which is based on the AnyOption Platform, is the most recommended binary options trading platform on the market.</p>\r\n\r\n<p>Between these two platforms, they are the simplest and user-friendly platforms out there today. Although some brokers do not, AnyOption decided to stay loyal to the traditional trading. This makes them more conservative than many of the modern brokers.</p>\r\n\r\n<p>Instead of creating new ways to trade, AnyOptions invested their resources in order to help develop additional trading tools. One of these tools is the Option+, which is a trading tool that offers its investors the option to delay or even close the expiry time prior to the actual closing time after purchasing the asset. The trading system itself is quiet simple, the trader views up to four assets which includes all the information needed at a time with simply a simple click. The additional market news feed is located below the tabs marked assets.</p>\r\n\r\n<p>AnyOption provides its customers with a profit line which is a simple but quiet efficient tool that keeps track of the assets purchased. Registering with the SMS service is another good way of tracing the assets. AnyOption sends you a text before or after the expiry time.</p>\r\n\r\n<p>For the people at Binary Options That Suck, the conservative approach that is used at AnyOptions, is the main reason that they don&rsquo;t suck. Even though the binary options trading industry is relatively still in their early years, Binary Options That Suck&rsquo;s expects more developments to be made in the industry. There are times when it is just simply better to stick to the classic brokers such as AnyOptions, in order to avoid the companies that are scams or simply sucky brokers.</p>\r\n\r\n<p>Scam or No Scam?</p>\r\n\r\n<p>Although AnyOptions is still a young website, it is already in the running if an award was being given for &ldquo;being the most inviting broker for beginners.&rdquo; AnyOptions would definitely be nominated, with an excellent chance of winning this one. In addition to the efficient platform and the greatest of learning tools for the beginning trader, they refund approximately 15% of any purchase in case of Out-Of-Money.</p>\r\n\r\n<p>Although AnyOptions refunds are among the best in the market, refunds are often critical for the traders that are just beginning. One of the downsides for AnyOptions is that although their Out-Of-Money percentage is good, their Out-Of-Money returns are only as high as 75% which is an average low in the market.</p>\r\n\r\n<p>AnyOptions doesn&rsquo;t tell its customers stories or build be fairytales and the fact is that AnyOptions actually lets its trader&rsquo;s trade without any initial bonus. This only goes to show just how much this company is legit. Broker companies in many cases, offer huge bonuses, which seem great at the time, but in actuality, these bonuses could prevent the traders from withdrawing their money back.</p>\r\n\r\n<p>A good example of this is when a no name broker offers to give you a 50% bonus on your initial deposit. This may sound like a good thing to you, but this is how it can turn around on you. Let&rsquo;s say you invest $200. This would give you a bonus of $100 in addition to you investment. Once you make some good trades and want to withdraw, that is when the problem starts.</p>\r\n\r\n<p>In this case, then the trader wants to withdraw his profits, he needs to trade between 10-15 times the equal value of the initial bonus, which in this case would amount to $1,000 to $1,500. The beginning trader often turns around and just loses their money because they don&rsquo;t succeed making so many profits. With AnyOptions this is not the case, if there is no bonus, a trader can just deposit and trade, with no strings attached.</p>\r\n\r\n<p>The key to the success of AnyOptions, is their conservative approach. Giving away a free 15% refund can only be done by lowering the return amounts. Although binary options markets are not yet fully recognized by all the governmental institutions, there are a few that are the most respected and have been granted with any kind of regulation. The AnyOptions owners have verified their information with Binary Options That Suck and it has been concluded that although they are not regulated with the government as of yet, they are safe.</p>\r\n\r\n<p>AnyOptions assets as of the end of 2011 and the beginning of 2012, are available for trade around the world. Within the United States, only stocks are available for trade being that AnyOptions have not yet completed their regulation there. After this regulation has been completed, all assets will be available for trade. A lot of people do not understand that a binary options broker actually limits the availability of its assets because they are not yet fully regulated.</p>\r\n\r\n<p>It is not necessary to be regulated in order to trade binary options. This is purely legal to do so. The fact that AnyOptions prevents the US customers from investing in those categories only show a tremendous effort on the part of AnyOptions to become fully recognized by the institutions. Once AnyOptions have received their full regulation inside the US, Binary Options That Suck, will certainly update all the readers. It is only fair to say though that AnyOptions is a good broker for the beginning trader and a safe place for the experienced trader who are continuously suspicious of binary options frauds.</p>\r\n\r\n<p>Complaints Received</p>\r\n\r\n<p>With the research process which Binaryoptionsthatsuck conducts, they look for broker complaints all over the web. When they find a complaint, they check it out and bring the results to the public. The process they use for this research is simple, they search on Google for any complaints regarding AnyOption. This can be related to scams, fraud, complaints or anything else customers have to say.</p>\r\n\r\n<p>When researching AnyOptions, the results were conflicting. They found comments in a variety of different blogs concerning AnyOption. Most of the comments that were found were in detail and some people even wrote over 200 words, detailing their issues. The majority of the complaints seemed to be from unhappy customers. Many of them had lost their money. A percentage of the complaints claimed that AnyOptions prevented them from trading again after winning. While this is what was found regarding AnyOptions, similar complaints on different websites and blogs have been seen, so it is a little difficult to tell whether or not these complaints are true. During the process of the research though, the absence of any complaint regarding the withdrawal issues is in the favor of AnyOptions.</p>\r\n\r\n<p>Bonus Or No Bonus</p>\r\n\r\n<p>It is not the usual for AnyOption to offer bonus cash to all its customers. The sales teams are given the bonuses on a case by case basis. These bonuses are usually negotiable. The bonus wager varies according to the deal which is set with the client. Once the bonus is given to the client, they must fulfill the agreed wager in order to be entitled for the full sum of the bonus. It is recommended that traders start with a low bonus. This will allow the trader that has problems fulfilling the wager requirements to be able to withdraw easier. AnyOption will reduce the bonus from the profits.</p>\r\n\r\n<p>AnyOption Withdrawal</p>\r\n\r\n<p>Most of the binary option brokers sites are more than happy to tell you how to deposit your money, so at binaryoptionsthatsuck.com, this is not a concern. The most important thing in binary options is how to withdraw your profits. Some of the binary option brokers cause problems for their traders when they attempt to withdraw their money. The good thing about AnyOptions is that they do not have a maximum withdrawal limit like some of the brokers do. The route of withdrawal is also available via the same methods used for depositing your money.</p>\r\n\r\n<p>Extras With AnyOption</p>\r\n\r\n<p>Everyone is looking for extras these days, with the economy of today, it is no wonder. Some traders are looking for extras with every broker, but at binaryoptionsthatsuck.com, they think differently.</p>\r\n\r\n<p>The extras that they link with AnyOptions is:</p>\r\n\r\n<p>A unique trading platform.</p>\r\n\r\n<p>Up to 25% refund.</p>\r\n\r\n<p>A really easy and great trading mobile app.</p>\r\n\r\n<p>Live quotes from Reuters.</p>\r\n\r\n<p>1-100 Options.</p>\r\n\r\n<p>ShowOff</p>\r\n\r\n<p>The Rating For AnyOption</p>\r\n\r\n<p>User-Friendly 17/20</p>\r\n\r\n<p>The AnyOption platform is 100% web based and requires not downloads for the customer. It is a simple, inviting, modern, and an overall user-friendly platform. There is learning material available on the site, making them compatible with beginners. The website is also available in 14 different languages.</p>\r\n\r\n<p>Number of Assets and Expiry Time 17/20</p>\r\n\r\n<p>Total number of assets are 97: including 60 stocks, 9 currencies (forex), 4 commodities, and 32 indices.</p>\r\n\r\n<p>Their variety of assets are considered above average, with 60 assets available for trade. Remember that the US customers can only trade stocks with AnyOptions at this time.</p>\r\n\r\n<p>The length of their expiry times are considered good, stretching from 15 minutes to a couple of day for most of their assets. This also includes the weekends.</p>\r\n\r\n<p>Commissions, Support and Effective return 18/20</p>\r\n\r\n<p>There is not a fee or commission charged by AnyOption to the trader when they deposit or purchase options. If more than one transaction per month is made, there is a $30 fee charged.</p>\r\n\r\n<p>AnyOption support is available with international numbers, including English. Their support is also available via e-mail. Live chat is considered the most efficient way to communicate for most of the brokers.</p>\r\n\r\n<p>The Out-Of-Money refund percentage for all assets is approximately 25%, while the In-The-Money refund percentage for all assets is approximately 64-80%.</p>\r\n\r\n<p>Deposit, Payment, And Bonus 17/20</p>\r\n\r\n<p>Minimum deposits are average at $200 or 200 pounds. Deposits can be made via C. C., wire transfer, Cash-U, Moneybookers, and others.</p>\r\n\r\n<p>Withdrawals are typically available via the same methods as the deposits. If more than one withdrawal is made per month, there is a $30 fee applied.</p>\r\n\r\n<p>AnyOptions offers a negotiable bonus to most their clients. Read paragraphs four and five of the Scam or No Scam section to help you understand more regarding the bonuses or visit the AnyOptions article section.</p>\r\n\r\n<p>Website Extra&rsquo;s 18/20</p>\r\n\r\n<p>AnyOptions has 0-100 Options, account manager and users trading data. Their innovative platform is very unique and can only be found on their website. This platform was created especially to meet the needs of the mother company.</p>\r\n\r\n<p>The AnyOption platform includes Call/Put Options, along with the Option+. The Option+ feature enables the trader to close or to extend the expiry time of assets that have already been purchased. AnyOptions also will provide its customers with an SMS service and other interesting features such as the ShowOff. Their app is also available on your mobile.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n','Anyoption review','Anyoption review','index','2014-02-20 03:49:28','2014-02-20 03:49:28',1);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-20  4:44:12
