<ul class="breadcrumbs">
    <li class="home"><a href="index-2.html">Home</a></li>
    <li class="all"><a href="page-style1.html">All Posts</a></li>
    <li class="cat_post"><a href="page-style1.html">News</a></li>
    <li class="current"></li>
</ul>
<h2 class="page-title"><?php echo $page->title;?></h2>
<div id="post_content" class="post_content" role="main">
    <article class="type-post hentry">
        <div class="post-info">
            <div class="post_date"><?php echo date('F d, Y',strtotime($page->created));?></div>
            <a href="post-standart.html" class="post_format">News</a> <a href="post-standart.html" class="comments_count">0</a>
            <div class="post_views"><?php echo $page->views;?></div>
        </div>
        <div class="pic post_thumb">
            <?php if ($page->medias) { ?><img src="/uploads/<?php echo $page->medias[0]->url;?>" alt="<?php echo $page->title;?>"><? } ?>
        </div>
        <div class="post_content">
            <?php echo $page->content;?>
        </div>
        <div class="block-social">
            <div class="soc_label">recommend to friends</div>
            <ul id="post_social_share" class="post_social_share">
                <li><a href="http://www.facebook.com/share.php?u=<?php echo urlencode('http://binaryoptionsforprofit.com/page/'.$page->slug);?>" class="facebook_link"><img src="/images/facebook-icon-big.png" class="facebook_icon" alt="facebook"></a></li>
                <li><a href="https://twitter.com/share?text=<?php echo urlencode($page->title);?>" class="twitter_link"><img src="/images/twitter-icon-big.png" class="twitter_icon" alt="twitter"></a></li>
                <li><a href="https://plusone.google.com/_/+1/confirm?url=<?php echo urlencode('http://binaryoptionsforprofit.com/page/'.$page->slug);?>" class="gplus_link"><img src="/images/gplus-icon-big.png" class="gplus_icon" alt="gplus"></a></li>
                <li><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode('http://binaryoptionsforprofit.com/page/'.$page->slug);?>" class="pinterest_link"><img src="/images/pinterest-icon-big.png" class="pinterest_icon" alt="pinterest"></a></li>
            </ul>
        </div>
    </article>
    <?php /*<div id="recent_posts">
        <h3 class="section_title"> Recent articles </h3>
        <div class="posts_wrapper">
            <article class="item_left">
                <div class="pic"> <a href="post-standart.html" class="w_hover img-link img-wrap"> <img src="/images_post/13-170x126.jpg" alt=""></a> </div>
                <h3><a href="post-standart.html">Curabitur enim dui, euismod convallis.</a></h3>
                <div class="post-info"> <a href="post-standart.html" class="post_date">May 30, 2013</a> <a href="post-standart.html" class="comments_count">2</a> </div>
            </article>
            <article class="item_right">
                <div class="pic"> <a href="post-standart.html" class="w_hover img-link img-wrap"> <img src="/images_post/23-170x126.jpg" alt=""></a> </div>
                <h3><a href="post-standart.html">Fermentum erat, vitae tincidunt quam.</a></h3>
                <div class="post-info"> <a href="post-standart.html" class="post_date">May 30, 2013</a> <a href="post-standart.html" class="comments_count">1</a> </div>
            </article>
        </div>
        <div class="posts_wrapper">
            <article class="item_left">
                <div class="pic"> <a href="post-standart.html" class="w_hover img-link img-wrap"> <img src="/images_post/18-170x126.jpg" alt=""></a> </div>
                <h3><a href="post-standart.html">Perfect For Your Business</a></h3>
                <div class="post-info"> <a href="post-standart.html" class="post_date">May 30, 2013</a> <a href="post-standart.html" class="comments_count">0</a> </div>
            </article>
            <article class="item_right">
                <div class="pic"> <a href="post-standart.html" class="w_hover img-link img-wrap"> <img src="/images_post/53-170x126.jpg" alt=""></a> </div>
                <h3><a href="post-standart.html">Praesent at erat eu metus luctus blandit.</a></h3>
                <div class="post-info"> <a href="post-standart.html" class="post_date">May 30, 2013</a> <a href="post-standart.html" class="comments_count">1</a> </div>
            </article>
        </div>
    </div> */?>
    <div id="comments" class="post_comments">
        <div id="respond">
            <?php if ($page->comments) {?>
            <h3 class="comments_title">Comments</h3>
            <ol class="comment-list">
                <?php foreach ($page->comments as $c) {?>
                <li class="comment">
                    <div class="photo"><img src="/images_post/b5a37fca93d6cc2e271d7866d896a785.png" alt=""></div>
                    <div class="extra_wrap">
                        <h5><a href="page-author.html"><?php echo $c->author_name;?></a></h5>
                        <div class="comment_info">
                            <div class="comment_date"><?php echo date('F d, Y \a\t H:i a',strtotime($c->date));?></div>
                            <div class="comment_reply_link"><a class="comment-reply-link">Reply</a></div>
                        </div>
                        <div class="comment_content">
                            <p><?php echo $c->text;?></p>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ol>
            <?php } ?>

                <?php

                $this->widget('CStarRating',array(
                    'name'=>'rating_4',
                    'value'=>'3',
                    'readOnly'=>(!Yii::app()->user->id) ? true : false,
                    'minRating'=>1,
                    'maxRating'=>5,
                    'cssFile'=>'',//need custom css file, default is bad
                    'callback'=>'
                        function(){
                            $.ajax({
                                type: "POST",
                                url: "'.Yii::app()->createUrl('/site/rate/&type=page&id='.$page->id).'",
                                    data: "star_rating=" + $(this).val(),
                                    success: function(data){
                                        $("#mystar_voting").html(data);
                                    }
                                })}',
                    'titles'=>array(
                        '1'=>'Normal',
                        '2'=>'Average',
                        '3'=>'OK',
                        '4'=>'Good',
                        '5'=>'Excellent'
                    ),
                ));
                if (!Yii::app()->user->id) echo 'You must be authorized to rate pages. Please <a href="">log in</a> or <a href="">register</a>';

                $this->renderPartial('/comment/_form',array(
                    'model'=>$commentForm,
                )); ?>
        </div>
        <!-- /#respond -->
        <div class="nav_comments"></div>
    </div>
</div>