<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>PrimeTime</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/html5.js"></script>
    <link rel="stylesheet" href="/style/ie.css" type="text/css" media="all">
    <![endif]-->
    <link rel="stylesheet" href="/style/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="/style/responsive.css" type="text/css" media="all">
    <?php /*<link rel="stylesheet" href="/css/form.css" type="text/css" media="all">*/?>
    <?php //<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>?>
</head>
<body class="boxed pattern-5">
<!--[if lt IE 8]>
<div class="sc_infobox sc_infobox_style_error">
    It looks like you're using an old version of Internet Explorer. For the best web site experience, please <a href="http://windows.microsoft.com/en-us/internet-explorer/ie-10-worldwide-languages">update your browser</a> or learn how to <a href="http://browsehappy.com">browse happy</a>!
</div>
<![endif]-->
<div id="page">

<!-- header begin -->
<header role="banner" class="site-header" id="header">
<div>
<?php /* ?>
<section class="top">
    <div class="inner clearboth">
        <div class="top-right">
            <ul id="user-links">
                <li><a href="#">Features</a></li>
                <li><a class="login-popup-link" href="#login">Login</a></li>
                <li><a class="registration-popup-link" href="#registration">Registration</a></li>
            </ul>
        </div>
        <div class="top-left">
            <div id="jclock1" class="simpleclock"></div>
        </div>
        <div class="top-center">
            <div class="block_top_menu">
                <ul id="top-left-menu" class="top-left-menu">
                    <li class="red"><a href="index-2.html">Home</a></li>
                    <li><a href="page-about.html">About us</a></li>
                    <li class="nomobile"><a href="#">Dropdown</a>
                        <ul>
                            <li><a href="#">Drop Menu1</a></li>
                            <li><a href="#">Drop Menu2</a></li>
                            <li><a href="#">Drop Menu3</a></li>
                        </ul>
                    </li>
                    <li><a href="shortcodes-typography.html">Typography</a></li>
                    <li><a href="page-contact.html">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php */ ?>
    <section class="section2">
    <div class="inner">
        <div class="section-wrap clearboth">
            <div class="block_social_top">
                <div class="icons-label">Follow us:</div>
                <ul>
                    <li><a class="tw" href="#" title="Twitter">Twitter</a></li>
                    <li><a class="fb" href="#" title="Facebook">Facebook</a></li>
                    <li><a class="rss" href="#" title="RSS">RSS</a></li>
                    <li><a class="gplus" href="#" title="Google+">Google+</a></li>
                    <?php /*<li><a class="vim" href="#" title="Vimeo">Vimeo</a></li>
                    <li><a class="tmb" href="#" title="Tumblr">Tumblr</a></li>*/ ?>
                    <li><a class="pin" href="#" title="Pinterest">Pinterest</a></li>
                </ul>
            </div>
            <div class="form_search">
                <form role="search" class="searchform" id="searchform" method="get" action="/search/">
                    <input type="search" placeholder="Search …" id="search" value="" name="q" class="field">
                    <input type="submit" value="Search" id="submit" class="submit">
                </form>
            </div>
            <div class="newsletter">
                <div class="newsletter-title">Subscribe to our newsletter</div>
                <div class="newsletter-popup"> <span class="bg"><span></span></span>
                    <div class="indents">
                        <form target="_blank" method="post" action="http://feedburner.google.com/fb/a/mailverify?">
                            <div class="field">
                                <input type="text" class="w_def_text" title="Enter Your Email Address" name="email" placeholder="Enter Your E-mail addres">
                            </div>
                            <input type="submit" value="Subscribe" class="button">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section3">
    <div class="section-wrap clearboth">
        <div class="banner-block">
            <div class="banner"> <a href="#"><img alt="banner" src="/images/banner.jpg"></a> </div>
        </div>
        <div class="name-and-slogan">
            <h1 class="site-title"><a rel="home" title="PrimeTime" href="/"> <img alt="logo" src="/images/logo.png"> </a></h1>
            <h2 class="site-description">Your guide in Binary Options world</h2>
        </div>
    </div>
</section>
<div class="headStyleMenu">
    <section class="section-nav">
        <nav role="navigation" class="navigation-main">
            <ul class="clearboth mainHeaderMenu">
                <li class="home"><a href="/"></a></li>
                <li class="red"><a href="index-2.html">Home</a>
                    <?php /*<ul>
                        <li><a href="index-2.html">Home with slider 1</a></li>
                        <li><a href="slider2.html">Home with slider 2</a></li>
                        <li><a href="slider3.html">Home with slider 3</a></li>
                    </ul>*/?>
                </li>
                <li class="blue"><a href="/binary-options-brokers">Rating & Reviews</a>
                    <?php /*<ul>
                        <li><a href="page-style1.html">Binary Options Brokers</a></li>
                        <li><a href="page-style2.html">Binary Options Signals</a></li>
                        <li><a href="page-style3.html">Binary Options Tools</a></li>
                    </ul>*/ ?>
                </li>
                <?php //<li class="purple"><a href="/site/binary-options-brokers">Best brokers</a>// ?>
                </li>
                <li class="sky-blue"><a href="/site/binary-options-strategies">Trading Strategies</a>
                </li>
                <?php //<li class="orange"><a href="page-contact.html">How to start</a></li> ?>
                <li class="yellow"><a href="/site/contact">Contact Us</a></li>
            </ul>
        </nav>
    </section>
    <!-- /#site-navigation -->
    <!-- mobile menu -->
    <section class="section-navMobile">
        <div class="mobileMenuSelect"><span class="icon"></span>Home</div>
        <ul class="clearboth mobileHeaderMenuDrop">
            <li><a href="#">Home</a>
                <ul>
                    <li><a href="index-2.html">Home with slider 1</a></li>
                    <li><a href="slider2.html">Home with slider 2</a></li>
                    <li><a href="slider3.html">Home with slider 3</a></li>
                </ul>
            </li>
            <li><a href="page-style1.html">Category</a>
                <ul>
                    <li><a href="page-style1.html">Category Page Style 1</a></li>
                    <li><a href="page-style2.html">Category Page Style 2</a></li>
                    <li><a href="page-style3.html">Category Page Style 3</a></li>
                </ul>
            </li>
            <li><a href="reviews-all.html">Reviews</a>
                <ul>
                    <li><a href="reviews-all.html">All Reviews</a></li>
                    <li><a href="reviews-item-page.html">Reviews item page</a></li>
                </ul>
            </li>
            <li><a href="post-standart.html">Post Formats</a>
                <ul>
                    <li><a href="post-slider-galery.html">Post Format With Slider</a></li>
                    <li><a href="post-slider-galery.html">Post Format With Gallery</a></li>
                    <li><a href="post-youtube.html">Post Format YouTube Video</a></li>
                    <li><a href="post-vimeo.html">Post Vimeo Video</a></li>
                    <li><a href="post-standart.html">Standard Post</a></li>
                </ul>
            </li>
            <li><a href="shortcodes-elements.html">Shortcodes</a>
                <ul>
                    <li><a href="shortcodes-elements.html">Elements</a></li>
                    <li><a href="shortcodes-columns.html">Columns</a></li>
                    <li><a href="shortcodes-typography.html">Typography</a></li>
                </ul>
            </li>
            <li><a href="page-blogs.html">Page templates</a>
                <ul>
                    <li><a href="page-about.html">About us</a></li>
                    <li><a href="page-author.html">Author Page</a></li>
                    <li><a href="page-full.html">Full Width page</a></li>
                    <li><a href="page-contact.html">Contact Us</a></li>
                    <li><a href="page-blogs.html">Blogs page</a></li>
                </ul>
            </li>
            <li><a href="media-photos.html">Media</a>
                <ul>
                    <li><a href="media-videos.html">All videos</a></li>
                    <li><a href="media-photos.html">All photos</a></li>
                </ul>
            </li>
            <li><a href="page-contact.html">Contact Us</a></li>
        </ul>
    </section>
    <!-- /mobile menu -->
</div>
<?php /* ?>
<section class="news-ticker">
    <!-- Recent News slider -->
    <div id="flexslider-news" class="header_news_ticker">
        <ul class="news slides" >
            <li><a href="post-standart.html">Cras dui tellus, auctor eget porttitor ac, imperdiet ut nibh. Fusce orci lorem, consequat vitae...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
            <li><a href="post-standart.html">Cras dui tellus, auctor eget porttitor ac, imperdiet ut nibh. Fusce orci lorem, consequat vitae...</a></li>
            <li><a href="post-standart.html">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam...</a></li>
        </ul>
    </div>
    <!-- /Recent News slider -->
</section>
<?php */ ?>
</div>
</header>
<!-- / header  -->
<div id="main" class="right_sidebar">
<div class="inner">
<div class="general_content clearboth">
<!--Post Blogs-->
<div class="main_content <?php if (isset($this->contentClass)) echo $this->contentClass;?>">
    <?php echo $content; ?>
</div>
<!-- .main_content -->

<!--/ .main_content -->
<!--/Post Blogs-->

<?php if ($this->hasSidebar) {?>
<!-- .main_sidebar -->
<div role="complementary" class="main_sidebar right_sidebar" id="secondary">
<aside class="widget widget_news_combine" id="news-combine-widget-2">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_news" href="page-style1.html">All news</a></div>
        <h3 class="widget_title">Main News</h3>
    </div>
    <div class="widget_body">
        <div class="block_news_tabs" id="tabs">
            <div class="tabs">
                <ul>
                    <li><a href="#tabs1"><span>Latest</span></a></li>
                    <li><a href="#tabs2"><span>Popular</span></a></li>
                    <li><a href="#tabs3"><span>Most Commented</span></a></li>
                </ul>
            </div>
            <!-- tab content goes here -->
            <div class="tab_content" id="tabs1">
                <?php foreach($this->widgets['news']['by_date'] as $row) { ?>
                <div class="block_home_news_post">
                    <div class="info">
                        <div class="date"><?php echo date('M d',strtotime($row->created));?></div>
                    </div>
                    <p class="title"><a href="/page/<?php echo $row->slug;?>"><?php echo $row->title;?></a></p>
                </div>
                <?php } ?>
            </div>
            <!-- tab content goes here -->
            <div class="tab_content" id="tabs2">
                <?php foreach($this->widgets['news']['by_views'] as $row) { ?>
                    <div class="block_home_news_post">
                        <div class="info">
                            <div class="date"><?php echo date('M d',strtotime($row->created));?></div>
                        </div>
                        <p class="title"><a href="/page/<?php echo $row->slug;?>"><?php echo $row->title;?></a></p>
                    </div>
                <?php } ?>
            </div>
            <!-- tab content goes here -->
            <div class="tab_content" id="tabs3">
                <?php foreach($this->widgets['news']['by_comments'] as $row) { ?>
                    <div class="block_home_news_post">
                        <div class="info">
                            <div class="date"><?php echo date('M d',strtotime($row->created));?></div>
                        </div>
                        <p class="title"><a href="/page/<?php echo $row->slug;?>"><?php echo $row->title;?></a></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</aside>
<?php /*<aside class="widget followers" id="followers-widget-2">
    <div class="block_subscribes_sidebar">
        <div class="service facebook_info"> <a class="fb" href="http://www.facebook.com/envato"> <span class="num">29076</span> <span class="people">Subs.</span> </a> </div>
        <div class="service twitter_info"> <a class="tw" href="http://www.twitter.com/envato"> <span class="num">0</span> <span class="people">Follow.</span> </a> </div>
        <div class="service"> <a class="rss" href="http://feeds.feedburner.com/envato"> <span class="num">300</span> <span class="people">Subs.</span> </a> </div>
    </div>
</aside>*/?>
<?php /* ?>
<!--Video Widget-->
<aside class="widget widget_recent_video">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="media-videos.html">All videos</a></div>
        <h3 class="widget_title">Video Widget</h3>
    </div>
    <div class="widget_body">
        <div id="video_carousel" class="thumb_carousel jcarousel-container jcarousel-container-vertical">
            <div class="jcarousel-container">
                <div class="jcarousel-clip jcarousel-clip-vertical" >
                    <ul class="jcarousel-list">
                        <li> <a data-content="#" data-href="/images_post/18-458x294.jpg" title="Video post example" href="http://www.youtube.com/watch?v=w5w0S13x2yQ"> <img alt="" src="/images_post/18-90x66.jpg"> </a> </li>
                        <li> <a data-content="#" data-href="/images_post/22-458x294.jpg" title="Perspiciatis unde omnis iste natus voluptatem." href="http://vimeo.com/67019026"> <img alt="" src="/images_post/22-90x66.jpg"> </a> </li>
                        <li> <a data-content="#" data-href="/images_post/24-458x294.jpg" title="Perspiciatis unde omnis iste natus voluptatem." href="http://vimeo.com/67019026"> <img alt="24" src="/images_post/24-90x66.jpg"> </a> </li>
                        <li> <a data-content="#" data-href="/images_post/25-458x294.jpg" title="Perspiciatis unde omnis iste natus voluptatem." href="http://vimeo.com/67019026"> <img alt="" src="/images_post/25-90x66.jpg"> </a> </li>
                    </ul>
                </div>
            </div>
            <div class="jcarousel-prev jcarousel-prev-vertical jcarousel-prev-disabled jcarousel-prev-disabled-vertical"><span></span></div>
            <div class="jcarousel-next jcarousel-next-vertical" ><span></span></div>
        </div>
        <div id="carousel_target" class="video-thumb"> <a class="w_hover img-link img-wrap prettyPhoto" href="http://www.youtube.com/watch?v=w5w0S13x2yQ"><img alt="" src="/images_post/18-458x294.jpg"><span class="v_link"></span></a>
            <div class="post_title"><a class="post_name" href="post-youtube.html">Video post example</a></div>
        </div>
    </div>
</aside>
<?php */ ?>

<!--Video Widget-->
<?php /* ?>
<aside class="widget widget_recent_blogposts">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="page-blogs.html">all recent posts</a></div>
        <h3 class="widget_title">Recent posts</h3>
    </div>
    <div class="widget_body">
        <ul class="slides">
            <li>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/13-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Curabitur enim dui, euismod convallis.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">704</a></li>
                                <li><a class="comments" href="post-standart.html">2</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/23-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Fermentum erat, vitae tincidunt quam.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">426</a></li>
                                <li><a class="comments" href="post-standart.html">1</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/18-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Perfect For Your Business</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">179</a></li>
                                <li><a class="comments" href="post-standart.html">0</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/53-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Praesent at erat eu metus luctus blandit.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">221</a></li>
                                <li><a class="comments" href="post-standart.html">1</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/61-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Fusce sagittis fermentum erat vitae.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">131</a></li>
                                <li><a class="comments" href="post-standart.html">0</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/18-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Audio post format</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">163</a></li>
                                <li><a class="comments" href="post-standart.html">0</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/33-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Nulla ullamcorper nisi odio, non tempor neque.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">144</a></li>
                                <li><a class="comments" href="post-standart.html">0</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="article">
                    <div class="pic"> <a class="w_hover img-link img-wrap" href="post-standart.html"><img  alt=""  src="/images_post/10-388x246.jpg"> </a> </div>
                    <div class="text">
                        <p class="title"><a href="post-standart.html">Aenean arcu lectus, porta eleifend vestibulum.</a></p>
                        <div class="icons">
                            <ul>
                                <li><a class="views" href="post-standart.html">117</a></li>
                                <li><a class="comments" href="post-standart.html">0</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="pages_info"> <span class="cur_page">1</span> of <span class="all_pages">1</span> </div>
    </div>
</aside>
<?php */ ?>
<?php /* ?>
<!-- Recent posts -->
<aside class="widget widget_recent_photos">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="media-photos.html">All photos</a></div>
        <h3 class="widget_title">Recent photos</h3>
    </div>
    <div class="widget_body">
        <div id="recent_photos_thumbs">
            <div class="flex-viewport" >
                <ul class="slides">
                    <li><img alt="" src="/images_post/13-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/23-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/18-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/53-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/61-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/18-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/33-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/10-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/22-88x86.jpg"></li>
                    <li><img alt="" src="/images_post/5-88x86.png"></li>
                    <li><img alt="" src="/images_post/7-88x86.png"></li>
                    <li><img alt="" src="/images_post/3-88x86.png"></li>
                </ul>
            </div>
            <ul class="flex-direction-nav">
                <li><a href="#" class="flex-prev flex-disabled"><span></span></a></li>
                <li><a href="#" class="flex-next"><span></span></a></li>
            </ul>
        </div>
        <!--Recent photos-->
        <div id="recent_photos_slider" >
            <div class="flex-viewport" >
                <ul class="slides" >
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/13.jpg"><img alt="" src="/images_post/13-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Curabitur enim dui, euismod convallis.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/23.jpg"><img alt="" src="/images_post/23-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Fermentum erat, vitae tincidunt quam.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/18.jpg"><img alt="" src="/images_post/18-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Perfect For Your Business</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/53.jpg"><img alt="" src="/images_post/53-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Praesent at erat eu metus luctus blandit.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/61.jpg"><img alt="" src="/images_post/61-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Fusce sagittis fermentum erat vitae.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/18.jpg"><img alt="" src="/images_post/18-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Audio post format</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/33.html"><img alt="" src="/images_post/33-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Nulla ullamcorper nisi odio, non tempor neque.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/10.jpg"><img alt="" src="/images_post/10-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Aenean arcu lectus, porta eleifend vestibulum.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/22.jpg"><img alt="" src="/images_post/22-552x292.jpg"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Tempor dolor nec lectus facilisis et consequat.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/5.png"><img alt="" src="/images_post/5-552x292.png"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Lectus facilisis et consequat lectus malesuada.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/7.png"><img alt="" src="/images_post/7-552x292.png"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Etiam sapien urna, mollis volutpat malesuada.</a></div>
                    </li>
                    <li><a class="gal_link w_hover img-link prettyPhoto" href="/images_post/3.png"><img alt="" src="/images_post/3-552x292.png"> <span class="link-icon"></span> </a>
                        <div class="title"><a href="#">Nunc ut metus eu leo ornare sagittis non commodo sem.</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</aside>
<?php */ ?>
<?php /* ?>
<aside class="widget twitter" >
    <div class="block_twitter_widget">
        <div class="widget_header blue">
            <div class="lnk_follow widget_subtitle"><a target="_blank" href="https://twitter.com/envato">Follow on Twitter</a></div>
            <h3 class="widget_title">Latest tweets</h3>
        </div>
        <div class="tweet widget_body">
            <div class="tweetBody" >
                <ul>
                    <li><a href="#">@wpspacethemes</a> How can our bank can get the corrected Name of the Receiver for our account, since it has not been specified correctly?</li>
                    <li><a href="#">@wpspacethemes</a> Check out this great #themeforest item 'PrimeTime - Clean, Responsive WP Magazine</li>
                </ul>
            </div>
        </div>
    </div>
</aside>
<!-- newsletter -->
<aside class="widget feedburner_subscribe" >
    <div class="block_newsletter">
        <div class="widget_header">
            <h3 class="widget_title">FeedBurner Widget</h3>
        </div>
        <div class="widget_body">
            <div class="label">Subscribe to our email newsletter.</div>
            <form target="_blank" method="post" action="http://feedburner.google.com/fb/a/mailverify?">
                <div class="field">
                    <input type="text" placeholder="Enter Your E-mail address" class="w_def_text" title="Enter Your Email Address" name="email">
                </div>
                <input type="submit" value="Subscribe" class="button">
            </form>
        </div>
    </div>
</aside>
<?php */ ?>
<?php /* ?>
<!--Latest reviews-->
<aside class="widget widget_recent_reviews" >
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="reviews-all.html">All reviews</a></div>
        <h3 class="widget_title">Latest reviews</h3>
    </div>
    <div class="widget_body">
        <div class="recent_reviews">
            <ul class="slides">
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="/images_post/22-176x128.jpg"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Tempor dolor nec lectus facilisis et consequat.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points4"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a> </div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="/images_post/5-176x128.png"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Lectus facilisis et consequat lectus malesuada.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points2"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a> </div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="/images_post/7-176x128.png"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Etiam sapien urna, mollis volutpat malesuada.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points5"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a> </div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="/images_post/3-176x128.png"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Nunc ut metus eu leo ornare sagittis non...</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points5"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a> </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</aside>
<?php */ ?>
<!--Latest comments-->
<aside class="widget widget_latest_comments" >
    <div class="widget_header">
        <h3 class="widget_title">Latest comments</h3>
    </div>
    <div class="widget_body">
        <ul class="comments">
            <?php $lastCommitWidgetElement=count($this->widgets['comments'])-1;
            $i=0;
            foreach($this->widgets['comments'] as $row) { ?>
                <li <?php if ($i==$lastCommitWidgetElement) echo 'class="last"';?>>
                    <div class="comment_author"><?php echo $row->author_name;?> on <a href="">Page or Broker</a></div>
                    <div class="comment_text"><?php echo $row->text;?></div>
                    <div class="comment_date"><?php echo date('H:i d M Y',strtotime($row->date));?></div>
                </li>
            <?php $i++; } ?>
        </ul>
    </div>
</aside>
</div>
<!-- /.main_sidebar -->
<?php } ?>

</div>
<!-- /.general_content -->
</div>
<!-- /.inner -->
</div>
<!-- /#main -->

<footer role="contentinfo" class="site-footer" id="footer">
    <?php /* ?>
    <section class="ft_section_1">
        <div class="footer-wrapper">
            <div class="col1">
                <div id="footer_logo"><a href="index-2.html"><img title="PrimeTime" alt="PrimeTime" src="/images/footer-logo.png"></a></div>
                <div class="footer_text"> Perspiciatis unde omnis iste natus error sit voluptatem accusantium mque laudantium, totam rem aperiam, eaque ipsa quae ab illo. </div>
                <div class="block_social_footer">
                    <ul>
                        <li><a class="tw" href="#" title="Twitter">Twitter</a></li>
                        <li><a class="fb" href="#" title="Facebook">Facebook</a></li>
                        <li><a class="rss" href="#" title="RSS">RSS</a></li>
                        <li><a class="gplus" href="#" title="Google+">Google+</a></li>
                        <li><a class="vim" href="#" title="Vimeo">Vimeo</a></li>
                        <li><a class="tmb" href="#" title="Tumblr">Tumblr</a></li>
                        <li><a class="pin" href="#" title="Pinterest">Pinterest</a></li>
                    </ul>
                </div>
            </div>
            <div class="col2">
                <div class="block_footer_widgets">
                    <div class="column">
                        <div id="calendar_wrap">
                            <table>
                                <caption>
                                    October 2013
                                </caption>
                                <thead>
                                <tr>
                                    <th title="Monday" scope="col">M</th>
                                    <th title="Tuesday" scope="col">T</th>
                                    <th title="Wednesday" scope="col">W</th>
                                    <th title="Thursday" scope="col">T</th>
                                    <th title="Friday" scope="col">F</th>
                                    <th title="Saturday" scope="col">S</th>
                                    <th title="Sunday" scope="col">S</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <td id="prev" colspan="3"><a title="View posts for May 2013" href="#">« May</a></td>
                                    <td class="pad">&nbsp;</td>
                                    <td class="pad" id="next" colspan="3">&nbsp;</td>
                                </tr>
                                </tfoot>
                                <tbody>
                                <tr>
                                    <td class="pad" colspan="1">&nbsp;</td>
                                    <td id="today">1</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td>4</td>
                                    <td>5</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>8</td>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>13</td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>15</td>
                                    <td>16</td>
                                    <td>17</td>
                                    <td>18</td>
                                    <td>19</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td>22</td>
                                    <td>23</td>
                                    <td>24</td>
                                    <td>25</td>
                                    <td>26</td>
                                    <td>27</td>
                                </tr>
                                <tr>
                                    <td>28</td>
                                    <td>29</td>
                                    <td>30</td>
                                    <td>31</td>
                                    <td colspan="3" class="pad">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="column">
                        <div class="widget_popular_footer">
                            <div class="widget_header">
                                <h3>Most Popular</h3>
                            </div>
                            <div class="widget_body">
                                <div class="article">
                                    <div class="pic"> <a class="w_hover" href="post-standart.html"><img alt="" src="/images_post/21-112x80.jpg"> </a> </div>
                                    <div class="text">
                                        <p class="title"><a href="post-standart.html">Perspiciatis unde omnis iste natus voluptatem.</a></p>
                                        <div class="icons">
                                            <ul>
                                                <li><a class="views" href="post-standart.html">1305</a></li>
                                                <li><a class="comments" href="post-standart.html">4</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="article">
                                    <div class="pic"> <a class="w_hover" href="post-standart.html"><img alt="" src="/images_post/23-112x80.jpg"> </a> </div>
                                    <div class="text">
                                        <p class="title"><a href="post-standart.html">Many desktop publishing packages and web page edit...</a></p>
                                        <div class="icons">
                                            <ul>
                                                <li><a class="views" href="post-standart.html">909</a></li>
                                                <li><a class="comments" href="post-standart.html">1</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="article">
                                    <div class="pic"> <a class="w_hover" href="post-standart.html"><img alt="" src="/images_post/22-112x80.jpg"> </a> </div>
                                    <div class="text">
                                        <p class="title"><a href="post-standart.html">Tempor dolor nec lectus facilisis et consequat.</a></p>
                                        <div class="icons">
                                            <ul>
                                                <li><a class="views" href="post-standart.html">746</a></li>
                                                <li><a class="comments" href="post-standart.html">1</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column last">
                        <h3>Flickr Widget</h3>
                        <div class="block_flickr_footer">
                            <div class="flickr_badge_image"> <a href="http://www.flickr.com/photos/we-are-envato/8954733698/"> <img title="Author Guilherme Salum (DD Studios) at work in his studio" alt="A photo on Flickr" src="../farm4.staticflickr.com/3820/8954733698_a2646a7642_s.jpg"> </a> </div>
                            <div class="flickr_badge_image"><a href="http://www.flickr.com/photos/we-are-envato/8953389435/"><img title="Checking out the outdoor space" alt="A photo on Flickr" src="../farm4.staticflickr.com/3685/8953389435_e5caf8d988_s.jpg"></a></div>
                            <div class="flickr_badge_image"><a href="http://www.flickr.com/photos/we-are-envato/8954585074/"><img title="The team listening to what the next few months holds for the company" alt="A photo on Flickr" src="../farm4.staticflickr.com/3795/8954585074_a38ff86602_s.jpg"></a></div>
                            <div class="flickr_badge_image"><a href="http://www.flickr.com/photos/we-are-envato/8954585316/"><img title="Selina and Collis" alt="A photo on Flickr" src="../farm3.staticflickr.com/2879/8954585316_60966c9a23_s.jpg"></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php */ ?>
    <section class="ft_section_2">
        <div class="footer-wrapper">
            <ul id="footer_menu">
                <li><a href="/">Home</a></li>
                <li><a href="/site/contact">Contact Us</a></li>
            </ul>
            <div class="copyright">
                <div class="footer_text">&copy; 2014. All Rights Reserved. </div>
            </div>
        </div>
    </section>
</footer>
<?php /* ?>
<!-- PopUp -->
<div id="overlay"></div>
<!-- Login form -->
<div class="login-popup popUpBlock" >
    <div class="popup"> <a class="close" href="#">×</a>
        <div class="content">
            <div class="title">Authorization</div>
            <div class="form">
                <form name="login_form" method="post">
                    <div class="col1">
                        <label for="log">Login</label>
                        <div class="field">
                            <input type="text" id="log" name="log">
                        </div>
                    </div>
                    <div class="col2">
                        <label for="pwd">Password</label>
                        <div class="field">
                            <input type="password" id="pwd" name="pwd">
                        </div>
                    </div>
                    <div class="extra-col">
                        <ul>
                            <li><a class="register-redirect" href="#">Registration</a></li>
                        </ul>
                    </div>
                    <div class="column button">
                        <input type="hidden" value="" name="redirect_to">
                        <a class="enter" href="#"><span>Login</span></a>
                        <div class="remember">
                            <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                            <label for="rememberme">Remember me</label>
                        </div>
                    </div>
                    <div class="soc-login">
                        <div class="section-title">Enter with social networking</div>
                        <div class="section-subtitle">Unde omnis iste natus error sit voluptatem.</div>
                        <ul class="soc-login-links">
                            <li class="tw"><a href="#"><em></em><span>With Twitter</span></a></li>
                            <li class="fb"><a href="#"><em></em><span>Connect</span></a></li>
                            <li class="gp"><a href="#"><em></em><span>With Google +</span></a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Login form -->

<!-- Registration form -->
<div class="registration-popup popUpBlock">
    <div class="popup"> <a class="close" href="#">×</a>
        <div class="content">
            <div class="title">Registration</div>
            <div class="form">
                <form name="registration_form" method="post" action="#">
                    <div class="col1">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_username">Name</label>
                            </div>
                            <div class="input-wrap">
                                <input type="text" id="registration_form_username" name="registration_form_username">
                            </div>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_email">Email</label>
                            </div>
                            <div class="input-wrap">
                                <input type="text" id="registration_form_email" name="registration_form_email">
                            </div>
                        </div>
                    </div>
                    <div class="col1">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_pwd1">Password</label>
                            </div>
                            <div class="input-wrap">
                                <input type="password" id="registration_form_pwd1" name="registration_form_pwd1">
                            </div>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_pwd2">Confirm Password</label>
                            </div>
                            <div class="input-wrap">
                                <input type="password" id="registration_form_pwd2" name="registration_form_pwd2">
                            </div>
                        </div>
                    </div>
                    <div class="extra-col">
                        <ul>
                            <li><a class="autorization-redirect" href="#">Autorization</a></li>
                        </ul>
                    </div>
                    <div class="column button"> <a class="enter" href="#"><span>Register</span></a>
                        <div class="notice">* All fields required</div>
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Registration form -->
<?php */ ?>
<!-- go Top-->
<a id="toTop" href="#"><span></span></a> </div>
<!--page-->

<script type="text/javascript" src="/js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="/js/superfish.js"></script>
<script type="text/javascript" src="/js/jquery.jclock.js"></script>
<script type="text/javascript" src="/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/js/jquery.elastislide.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/js/googlemap_init.js"></script>
<script type="text/javascript" src="/js/mediaelement.min.js"></script>
<script type="text/javascript" src="/js/lib.js"></script>
</body>
<?php //var_dump($this->widgets['news']['by_comments']);?>
</html>
