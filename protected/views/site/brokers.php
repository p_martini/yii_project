<?php
/*$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$brokers,
    'columns'=>array(
        'name',
        'regulation',
        array(
            'name'=>'Minimum deposit',
            'value'=>'$data->mindep'
        ),
        array(
            'name'=>'Return/Refund',
            'value'=>'$data->returnrefund',
            //'sortable'=>true
        ),
        array(
            'name'=>'Our score',
            //'value'=>'print(10)'
        ),
        array(
            'name'=>'Customer score',
        ),
        array(
            'name'=>'Reviewers'
        ),
        array(
            'name'=>'Links',
            'type'=>'raw',
            'value'=>'CHtml::link("Review",array("/page/".$data->slug."-review"))'
        )
    )
));*/

$this->widget('booster.widgets.TbGridView', array(
    'dataProvider'=>$brokers,
    'columns'=>array(
        'name',
        'regulation',
        'mindep',
        'returnrefund'.
        'our_score',
        array(
            'name'=>'Customer score',
        ),
        array(
            'name'=>'Reviewers'
        ),
        array(
            'name'=>'Links',
            'type'=>'raw',
            'value'=>'CHtml::link("Review",array("/page/".$data->slug."-review"))'
        )
    )
));

/*$this->widget(
    'booster.widgets.TbExtendedGridView',
    array(
        'type' => 'striped bordered',
        'dataProvider' => new CActiveDataProvider('Broker', array(
            'criteria' => array(
                'limit' => 10,
            )
        )),
        'template' => "{items}",
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '#',
                'htmlOptions' => array('style' => 'width: 60px')
            ),
            array('name' => 'country_code', 'header' => 'Country Code'),
            array(
                'name' => 'name',
                'header' => 'Region Name',
                'class' => 'booster.widgets.TbEditableColumn',
                'headerHtmlOptions' => array('style' => 'width:200px'),
                'editable' => array(
                    'type' => 'text',
                    'url' => '/example/editable'
                )
            ),
            array(
                'htmlOptions' => array('nowrap' => 'nowrap'),
                'class' => 'booster.widgets.TbButtonColumn',
                'viewButtonUrl' => null,
                'updateButtonUrl' => null,
                'deleteButtonUrl' => null,
            )
        )
    )
);*/


?>