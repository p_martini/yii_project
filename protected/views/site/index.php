<ul class="breadcrumbs">
    <li class="home"><a href="index-2.html">Home</a></li>
    <li class="current">Blog Posts</li>
</ul>
<h2 class="page-title">Blog Posts</h2>
<section id="blog_posts">
    <?php
    $countPosts=count($posts);
    if (count($countPosts)) {
        foreach ($posts as $p) {
    ?>
    <article>
        <?php if ($p->medias) { ?>
        <div class="pic">
            <a href="/page/<?php echo $p->slug;?>" class="w_hover img-link img-wrap">
                <img src="/uploads/<?php echo $p->medias[0]->url;?>" alt="">
                <span class="link-icon"></span>
            </a>
        </div>
        <?php } ?>
        <h3><a href="/page/<?php echo $p->slug;?>"><?php echo $p->title;?></a></h3>
        <div class="post-info"><a class="post_submitted">Posted by admin</a><a class="post_date"><?php echo date($p->created);?></a> <a class="comments_count">2</a></div>
        <div class="text"><?php echo substr($p->content,0,200);?>...</div>
        <a href="/page/<?php echo $p->slug;?>" class="more-link">Read More<span></span></a>
    </article>
    <?php }
     }?>

    <div id="nav_pages" class="nav_pages">
        <?php
        $this->widget('CLinkPager', array(
                'currentPage'=>$pages->getCurrentPage(),
                'itemCount'=>$pageCount,
                'pageSize'=>$pageSize,
                'maxButtonCount'=>5,
                'nextPageLabel' => 'Next',
                'prevPageLabel' => 'Prev',
                'selectedPageCssClass' => 'current',
                'hiddenPageCssClass' => 'disabled',
                'header'=>'',
                'htmlOptions'=>array('class'=>'pages'),
            )
        ); ?>
        <?php //<div class="page_x_of_y">Page <span>1</span> of <span>5</span></div> ?>
    </div>
</section>