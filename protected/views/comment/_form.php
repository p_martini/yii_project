<?php
/* @var $this SiteController */
/* @var $model CommentForm */
/* @var $form CActiveForm */

?>

<h3 id="reply-title">Leave comment<a name="comment"></a></h3>

<?php if(Yii::app()->user->hasFlash('createComment')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('createComment'); ?>
</div>

<?php else: ?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commentform',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'action'=>'/page/'.Yii::app()->request->getParam('slug').'/#comment'
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php /*<div class="result sc_infobox sc_infobox_style_error">
        <?php echo $form->errorSummary($model); ?>
        <p class="error_item" style="display:none;">The author name can't be empty</p>
    </div> */ ?>

	<div class="row comment-form-author">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row comment-form-email">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row comment-form-comment">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php //if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php //endif; ?>

	<div class="row buttons form-submit">
		<?php echo CHtml::submitButton('Post comment'); ?>
	</div>

    <?php /*<form method="post" id="commentform" style="display:none;">
        <p class="comment-form-author">
            <input id="author" name="author" type="text" value="" size="30">
            <label for="author" class="required">Name</label>
        </p>
        <p class="comment-form-email">
            <input id="email" name="email" type="text" value="" size="30">
            <label for="email" class="required">Email</label>
        </p>
        <p class="comment-form-url">
            <input id="url" name="url" type="text" value="" size="30">
            <label for="url">Website</label>
        </p>
        <p class="comment-form-comment">
            <textarea id="comment" name="comment" cols="45" rows="8"></textarea>
        </p>
        <p class="form-submit">
            <input name="submit" type="submit" id="submitComm" value="Post comment">
        </p>
    </form> */ ?>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>