jQuery(document).ready(function($){
    $(".slug_source").keyup(function(){
        var slugcontent = $(this).val();
        var slugcontent_hyphens = slugcontent.replace(/\s/g,'-');
        var finishedslug = slugcontent_hyphens.replace(/[^a-zA-Z0-9\-]/g,'').toLowerCase();
        $(".slug").val(finishedslug);
    });
});