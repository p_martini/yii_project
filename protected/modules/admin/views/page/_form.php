<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <?php if (isset($model->slug)) echo '<a href="/page/'.$model->slug.'/" target="_blank">View page on the site</a><br/><br/>'; ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100,'class'=>'slug_source')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>100,'class'=>'slug')); ?>
		<?php echo $form->error($model,'slug'); ?>
	</div>

	<div class="row textarea">
		<?php echo $form->labelEx($model,'content'); ?>
        <?php
        $this->widget('ext.editMe.widgets.ExtEditMe', array(
            'model' => $model,
            'attribute' => 'content'
        ));
        ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'category'); ?>
        <?php echo $form->dropDownList($model,'category',CHtml::listData(Category::model()->findAll(array('order'=>'name ASC')),'id','name'),array('empty'=>array('0'=>''))); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>160)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywords'); ?>
		<?php echo $form->textArea($model,'keywords',array('size'=>60,'maxlength'=>69)); ?>
		<?php echo $form->error($model,'keywords'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'view'); ?>
        <?php echo $form->textField($model,'view',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'view'); ?>
    </div>


    <div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
        <?php echo $form->dropDownList($model,'active',array('0'=>'no','1'=>'yes')); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->