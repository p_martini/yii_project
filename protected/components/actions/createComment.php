<?php
/* @var $model Comment */

class CreateComment extends CAction {
    public $returnUrl;
    public function actionCreateComment()
    {
        $controller=$this->getController();
        $model=new CommentForm;
        if(isset($_POST['CommentForm']))
        {
            $model->attributes=$_POST['CommentForm'];
            if($model->validate())
            {
                $model->save(false);
                Yii::app()->user->setFlash('createComment','Thank you for your comment. Your comment will be posted once it is approved.');
            }
        }
        $controller->redirect($this->returnUrl);
    }
}