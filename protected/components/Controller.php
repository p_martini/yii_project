<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */

    public $hasSidebar=true;
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    public $widgets=array();
    public $contentClass='';

    /* Get data for sidebar widgets */
    public function init() {
        $news=Yii::app()->cache->get('news');
        if (!$news) {
            $news['by_date']=Page::model()->findAll(array('order'=>'created DESC','limit'=>7));
            $news['by_views']=Page::model()->findAll(array('order'=>'views DESC','limit'=>7));
            $news['by_comments']=Page::model()->with('comments')->findAll(array(
                'select'=>'t.*',
                'order' => 'count(comments.id) desc',
                'group' => 't.id',
            ));
            Yii::app()->cache->set('news',$news,600);
        }
        $this->widgets['news']=$news;
        $comments=Yii::app()->cache->get('comments');
        if (!$comments) {
            $comments=Comment::model()->findAll(array('order'=>'date DESC','limit'=>5));
            Yii::app()->cache->set('comments',$comments,600);
        }
        $this->widgets['comments']=$comments;
    }
}