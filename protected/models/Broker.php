<?php

/**
 * This is the model class for table "broker".
 *
 * The followings are the available columns in table 'broker':
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property integer $review
 * @property integer $education_review
 * @property integer $platform_review
 * @property integer $type
 * @property string $afflink
 * @property string $livechat
 * @property string $updated
 * @property string $returnrefund
 * @property integer $mindep
 * @property string $ss1
 * @property string $ss2
 * @property integer $established
 * @property integer $us
 * @property integer $demo
 * @property string $based
 * @property integer $regulation
 * @property string $platlang
 * @property integer $return
 * @property integer $retund
 * @property double $score
 * @property double $our_score
 * @property string $assets
 * @property string $tradehrs
 * @property string $mintrade
 * @property string $maxtrade
 * @property string $opttypes
 * @property string $tools
 * @property string $quotes
 * @property integer $mobile
 * @property integer $smsalert
 * @property integer $emailalert
 * @property string $withdrawr
 * @property string $withdrawf
 * @property string $depopt
 * @property string $contactbroker
 * @property string $langs
 * @property string $phonenr
 * @property string $emailaddr
 * @property string $bonus
 */
class Broker extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'broker';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, slug', 'required'),
			array('review, education_review, platform_review, type, mindep, established, us, demo, regulation, return, retund, mobile, smsalert, emailalert', 'numerical', 'integerOnly'=>true),
			array('score, our_score', 'numerical'),
			array('name, slug, platlang, assets, mintrade, maxtrade, opttypes, withdrawr, withdrawf, depopt, langs, emailaddr', 'length', 'max'=>100),
			array('logo, livechat', 'length', 'max'=>255),
			array('returnrefund, ss1, ss2, based, tradehrs, tools, quotes, contactbroker, phonenr, bonus', 'length', 'max'=>45),
			array('afflink, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, slug, logo, review, education_review, platform_review, type, afflink, livechat, updated, returnrefund, mindep, ss1, ss2, established, us, demo, based, regulation, platlang, return, retund, score, our_score, assets, tradehrs, mintrade, maxtrade, opttypes, tools, quotes, mobile, smsalert, emailalert, withdrawr, withdrawf, depopt, contactbroker, langs, phonenr, emailaddr, bonus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'slug' => 'Slug',
			'logo' => 'Logo',
			'review' => 'Review',
			'education_review' => 'Education Review',
			'platform_review' => 'Platform Review',
			'type' => 'Type',
			'afflink' => 'Afflink',
			'livechat' => 'Livechat',
			'updated' => 'Updated',
			'returnrefund' => 'Return/Refund',
			'mindep' => 'Minimum Deposit',
			'ss1' => 'Ss1',
			'ss2' => 'Ss2',
			'established' => 'Established',
			'us' => 'Us',
			'demo' => 'Demo',
			'based' => 'Based',
			'regulation' => 'Regulation',
			'platlang' => 'Platlang',
			'return' => 'Return',
			'retund' => 'Retund',
			'score' => 'Score',
            'our_score' => 'Our Score',
			'assets' => 'Assets',
			'tradehrs' => 'Tradehrs',
			'mintrade' => 'Mintrade',
			'maxtrade' => 'Maxtrade',
			'opttypes' => 'Opttypes',
			'tools' => 'Tools',
			'quotes' => 'Quotes',
			'mobile' => 'Mobile',
			'smsalert' => 'Smsalert',
			'emailalert' => 'Emailalert',
			'withdrawr' => 'Withdrawr',
			'withdrawf' => 'Withdrawf',
			'depopt' => 'Depopt',
			'contactbroker' => 'Contactbroker',
			'langs' => 'Langs',
			'phonenr' => 'Phonenr',
			'emailaddr' => 'Emailaddr',
			'bonus' => 'Bonus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('review',$this->review);
		$criteria->compare('education_review',$this->education_review);
		$criteria->compare('platform_review',$this->platform_review);
		$criteria->compare('type',$this->type);
		$criteria->compare('afflink',$this->afflink,true);
		$criteria->compare('livechat',$this->livechat,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('returnrefund',$this->returnrefund,true);
		$criteria->compare('mindep',$this->mindep);
		$criteria->compare('ss1',$this->ss1,true);
		$criteria->compare('ss2',$this->ss2,true);
		$criteria->compare('established',$this->established);
		$criteria->compare('us',$this->us);
		$criteria->compare('demo',$this->demo);
		$criteria->compare('based',$this->based,true);
		$criteria->compare('regulation',$this->regulation);
		$criteria->compare('platlang',$this->platlang,true);
		$criteria->compare('return',$this->return);
		$criteria->compare('retund',$this->retund);
		$criteria->compare('score',$this->score);
        $criteria->compare('our_score',$this->our_score);
		$criteria->compare('assets',$this->assets,true);
		$criteria->compare('tradehrs',$this->tradehrs,true);
		$criteria->compare('mintrade',$this->mintrade,true);
		$criteria->compare('maxtrade',$this->maxtrade,true);
		$criteria->compare('opttypes',$this->opttypes,true);
		$criteria->compare('tools',$this->tools,true);
		$criteria->compare('quotes',$this->quotes,true);
		$criteria->compare('mobile',$this->mobile);
		$criteria->compare('smsalert',$this->smsalert);
		$criteria->compare('emailalert',$this->emailalert);
		$criteria->compare('withdrawr',$this->withdrawr,true);
		$criteria->compare('withdrawf',$this->withdrawf,true);
		$criteria->compare('depopt',$this->depopt,true);
		$criteria->compare('contactbroker',$this->contactbroker,true);
		$criteria->compare('langs',$this->langs,true);
		$criteria->compare('phonenr',$this->phonenr,true);
		$criteria->compare('emailaddr',$this->emailaddr,true);
		$criteria->compare('bonus',$this->bonus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Broker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
