<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property integer $category
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $description
 * @property string $keywords
 * @property string $view
 * @property string $created
 * @property string $updated
 * @property string $views
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Media[] $medias
 * @property Category $category0
 */
class Page extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category, title, slug, content', 'required'),
			array('category, active, views', 'numerical', 'integerOnly'=>true),
			array('title, slug', 'length', 'max'=>100),
			array('description', 'length', 'max'=>160),
			array('keywords', 'length', 'max'=>69),
			array('view', 'length', 'max'=>20),
			array('created, updated, views', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category, title, slug, content, description, keywords, view, created, updated, views, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'page_id'), //'order'=>'comment.date DESC'
            'commentCount' => array(self::STAT, 'Comment', 'page_id'),
			'medias' => array(self::HAS_MANY, 'Media', 'page'),
			'category0' => array(self::BELONGS_TO, 'Category', 'category'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'title' => 'Title',
			'slug' => 'Slug',
			'content' => 'Content',
			'description' => 'Description',
			'keywords' => 'Keywords',
			'view' => 'View',
			'created' => 'Created',
			'updated' => 'Updated',
            'views' => 'Views',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
        $criteria->compare('views',$this->views,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created = new CDbExpression('NOW()');
        else
            $this->updated = new CDbExpression('NOW()');

        return parent::beforeSave();
    }
}
