<?php

/**
 * This is the model class for table "comment".
 *
 * The followings are the available columns in table 'comment':
 * @property string $id
 * @property string $page_id
 * @property string $uid
 * @property string $text
 * @property string $ip
 * @property string $date
 * @property integer $status
 * @property string $author_name
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Page $page
 */
class Comment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, text, ip, date', 'required'),
			array('page_id, status', 'numerical', 'integerOnly'=>true),
			array('uid', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_id, uid, text, ip, date, status, author_name, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'uid' => 'Uid',
			'text' => 'Text',
			'ip' => 'Ip',
			'date' => 'Date',
			'status' => 'Status',
            'author_name'=>'Author Name',
            'email'=>'Author Email'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('ip',$this->ip);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);
        $criteria->compare('author_name',$this->author_name);
        $criteria->compare('email',$this->email);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function beforeSave() {

        if ($this->isNewRecord) {
            $this->ip = new CDbExpression('INET6_ATON("'.$this->ip.'")');
        }
        return parent::beforeSave();

    }
}
