<?php

class PageController extends Controller
{
    public function init(){
        parent::init();
        $this->contentClass='single ';
    }
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            /*'createComment'=>array(
                'class'=>'application.components.actions.createComment',
                'returnUrl'=>'/page/'.Yii::app()->request->getParam('slug')
            )*/
        );
    }
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'captcha'),
                'users'=>array('*')
            )
        );
    }
    public function actionIndex()
	{
        $slug=Yii::app()->request->getParam('slug');
        $page=Page::model()->with('medias')->with('comments')->findByAttributes(array('slug'=>$slug));
        if ($page) {
            $page->views++;
            $page->save(false);
            $commentForm=new CommentForm;
            if(isset($_POST['CommentForm']))
            {
                $commentForm->attributes=$_POST['CommentForm'];
                if($commentForm->validate())
                {
                    $comment=new Comment;
                    $comment->ip=$_SERVER['REMOTE_ADDR'];
                    $comment->page_id=$page->id;
                    $comment->text=strip_tags($commentForm->body);
                    $comment->email=$commentForm->email;
                    $comment->author_name=$commentForm->name;
                    $comment->date=date('Y-m-d H:i:s');
                    if (!$comment->save()) {
                        var_dump($comment->errors);exit;
                    }
                    Yii::app()->user->setFlash('createComment','Thank you for your comment. Your comment will be posted once it is approved.');
                }
            }
            $this->render('index',array(
                'page'=>$page,
                'commentForm'=>$commentForm
            ));
        } else {
            throw new CHttpException(404,'Page does not exist');
        }
	}
}